# Planet Raider

You play a treasure hunter also called "Planet Raider". The game starts on your native planet of your character, 
	during the funeral of your dad (a "Planet Raider" as well), deceased in front of you a few days ago, 
	murdered by a group of bloodthirsty "Planet Raiders".
	These treasure hunters, feared by all, have nothing but one goal : Establishing chaos in the Universe.

Before dying from his wounds, your father revealed the existence of an almighty treasure : "The Stone of the Soul". 
	The legend says that whoever would possess it would be granted an infinite power. 
	With his dying breath, your father gave you a map, showing the location of the stone, and instructions to reach it.
	
Your journey will take you to different planets where you will follow the directions left by your father, 
	while escaping the ennemies on your heels. 
	Throughout your adventure, you will learn more about your father's past, "The Stone of the Soul",
	and the real intentions of your pursuers.

## Getting Started

If you want to get a copy of the project up and running on your local machine for development and testing purposes :

```
git clone https://gitlab.com/NicolasHubs/planet-raider.git
```

### Prerequisites

You only need **Unreal Engine** in order to run our project :

* [**Download Unreal Engine**](https://www.unrealengine.com/en-US/download)

## Some pictures of our project

![Alt text](https://www.zupimages.net/up/18/26/tdfp.png "Main details")

## Built With

### Development Tools

* [Unreal Engine](https://www.unrealengine.com/en-US/feed) - The IDE we decided to use

### Organizational Tools

* [GoogleDoc](https://www.google.fr/intl/fr/docs/about/) - Mainly used to write in detail our game concept

## Authors

* **Croq Nicolas** - *Initial work* - [NicolasHubs](https://gitlab.com/NicolasHubs/)
* **Sammani Axel** - *Initial work* -
* **Merrien Maxime** - *Initial work* -